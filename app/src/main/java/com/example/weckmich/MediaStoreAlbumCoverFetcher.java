package com.example.weckmich;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import java.io.FileNotFoundException;

class MediaStoreAlbumCoverFetcher implements AlbumCoverFetcher {
    private static final String CONTENT_URI_ALBUM_ART = "content://media/external/audio/albumart";

    private ContentResolver contentResolver;

    MediaStoreAlbumCoverFetcher(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    @Override
    public Bitmap getAlbumCover(AlarmSound alarmSound) throws FileNotFoundException {
        Uri albumContentUri = Uri.parse(CONTENT_URI_ALBUM_ART);
        Uri albumCoverUri = ContentUris.withAppendedId(albumContentUri, alarmSound.getAlbumId());
        return BitmapFactory.decodeStream(contentResolver.openInputStream(albumCoverUri));
    }
}
