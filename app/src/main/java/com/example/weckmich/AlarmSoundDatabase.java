package com.example.weckmich;

/**
 * Persistence layer for alarm sounds
 */
interface AlarmSoundDatabase {
    /**
     * Store alarm sound to database
     */
    void saveAlarmSound(AlarmSound alarmSound);

    /**
     * Get latest alarm sound or null if no alarm sound was stored
     */
    AlarmSound getLatestAlarmSound();
}