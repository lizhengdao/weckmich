package com.example.weckmich;

class AlarmSoundAdapterException extends RuntimeException {
    public AlarmSoundAdapterException(String message) {
        super(message);
    }
}
