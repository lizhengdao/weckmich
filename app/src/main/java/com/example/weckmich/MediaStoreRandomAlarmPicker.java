package com.example.weckmich;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.util.Random;

class MediaStoreRandomAlarmPicker implements RandomAlarmPicker {
    private ContentResolver contentResolver;

    MediaStoreRandomAlarmPicker(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    @Override
    public AlarmSound drawRandomAlarmSound() {
        try (Cursor cursor = queryCandidates()) {
            return pickRandomAlarmSound(cursor);
        }
    }

    private Cursor queryCandidates() {
        String[] projection = new String[]{
                MediaStore.Audio.AudioColumns._ID,
                MediaStore.Audio.AudioColumns.ARTIST,
                MediaStore.Audio.AudioColumns.TITLE,
                MediaStore.Audio.AlbumColumns.ALBUM_ID,
                MediaStore.Audio.AudioColumns.IS_ALARM,
                MediaStore.Audio.Media.DATA
        };

        Uri contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        return contentResolver.query(contentUri,
                projection,
                null,
                null,
                null);
    }

    private AlarmSound pickRandomAlarmSound(Cursor cursor) {
        if (cursor.getCount() <= 0) {
            throw new RandomAlarmPickerException("Could not find any possible alarm sounds");
        }

        int randomIndex = new Random().nextInt(cursor.getCount());
        moveCursorFromStartToPosition(cursor, randomIndex);

        return convertToAlarmSound(cursor);
    }

    private void moveCursorFromStartToPosition(Cursor cursor, int position) {
        cursor.moveToFirst();
        boolean cursorMoveSuccess = cursor.move(position);

        if (!cursorMoveSuccess) {
            throw new RandomAlarmPickerException(
                    String.format("Could not move cursor to offset. Current position: %s, offset: %s, count: %s",
                            cursor.getPosition(),
                            position,
                            cursor.getCount()));
        }
    }

    private AlarmSound convertToAlarmSound(Cursor cursor) {
        int idColumn = cursor.getColumnIndex(MediaStore.Audio.AudioColumns._ID);
        int artistColumn = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.ARTIST);
        int titleColumn = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.TITLE);
        int albumColumn = cursor.getColumnIndex(MediaStore.Audio.AlbumColumns.ALBUM_ID);
        int isAlarmColumn = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.IS_ALARM);
        int pathColumn = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);

        boolean isAlarm = cursor.getInt(isAlarmColumn) != 0 ? true : false;

        return AlarmSound.create(cursor.getLong(idColumn),
                cursor.getString(artistColumn),
                cursor.getString(titleColumn),
                cursor.getInt(albumColumn),
                isAlarm,
                cursor.getString(pathColumn),
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
    }
}
