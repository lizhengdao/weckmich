package com.example.weckmich;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

class SimplePermissionManager implements PermissionManager {
    private Activity activity;

    SimplePermissionManager(Activity activity) {
        this.activity = activity;
    }

    @Override
    public boolean hasPermission(String permission) {
        return ContextCompat
                .checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void queryPermission(String permission, int permissionRequestCode) {
        ActivityCompat.requestPermissions(activity, new String[]{permission}, permissionRequestCode);
    }

    @Override
    public boolean hasSettingsWritePermission() {
        return Settings.System.canWrite(activity);
    }

    @Override
    public void querySettingsWritePermission() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
        intent.setData(Uri.parse(String.format("package:%s", BuildConfig.APPLICATION_ID)));
        activity.startActivity(intent);
    }
}
