package com.example.weckmich;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.MediaPlayer;

class AlarmSoundMediaPlayer implements AlarmSoundPlayer {
    private Context context;
    private MediaPlayer mediaPlayer;
    private AlarmSound currentSound;
    private OnCompletionListener completionListener;

    AlarmSoundMediaPlayer(Context context) {
        this.context = context;
    }

    @Override
    public void play() {
        if (canStartMediaPlayer()) {
            try {
                mediaPlayer.start();
            } catch (Exception e) {
                throw new AlarmSoundPlayerException("Could not play sound. See cause", e);
            }
        }
    }

    private boolean canStartMediaPlayer() {
        return currentSound != null && !isPlaying();
    }

    @Override
    public void stop() {
        if (isPlaying()) {
            mediaPlayer.pause();
            mediaPlayer.seekTo(0);
        }
    }

    @Override
    public boolean isPlaying() {
        if (mediaPlayer != null) {
            try {
                return mediaPlayer.isPlaying();
            } catch (Exception e) {
                return false;
            }
        }

        return false;
    }

    @Override
    public void setAlarmSound(AlarmSound alarmSound) {
        currentSound = alarmSound;

        setSoundForMediaPlayer(alarmSound);
    }

    @Override
    public void release() {
        currentSound = null;

        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    @Override
    public void setOnCompletionListener(OnCompletionListener listener) {
        completionListener = listener;
    }

    private void notifyCompletionListener() {
        if (completionListener != null) {
            completionListener.OnCompletion();
        }
    }

    private void setSoundForMediaPlayer(AlarmSound alarmSound) {
        if (mediaPlayer == null) {
            initializeMediaPlayer();
        }

        mediaPlayer.reset();

        setMediaPlayerDataSource(alarmSound);

        prepareMediaPlayer();
    }

    private void initializeMediaPlayer() {
        mediaPlayer = new MediaPlayer();

        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .build();

        mediaPlayer.setAudioAttributes(audioAttributes);
        mediaPlayer.setOnCompletionListener((MediaPlayer sender) -> {
            notifyCompletionListener();
        });
    }

    private void setMediaPlayerDataSource(AlarmSound alarmSound) {
        try {
            mediaPlayer.setDataSource(context, alarmSound.getContentUri());
        } catch (Exception e) {
            throw new AlarmSoundPlayerException("Could not set media player's data source. See cause", e);
        }
    }

    private void prepareMediaPlayer() {
        try {
            mediaPlayer.prepare();
        } catch (Exception e) {
            throw new AlarmSoundPlayerException("Could not prepare media player. See cause", e);
        }
    }
}
