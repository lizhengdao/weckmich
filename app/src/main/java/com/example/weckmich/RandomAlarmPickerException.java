package com.example.weckmich;

class RandomAlarmPickerException extends RuntimeException {
    RandomAlarmPickerException(String message) {
        super(message);
    }
}
