package com.example.weckmich;

/**
 * Exception to be thrown if the default alarm sound is set to none
 */
class NoneAlarmSoundException extends AlarmSoundAdapterException {
    public NoneAlarmSoundException(String message) {
        super(message);
    }
}
