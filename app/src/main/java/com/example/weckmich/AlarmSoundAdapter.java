package com.example.weckmich;

interface AlarmSoundAdapter {
    /**
     * Set given alarm sound as mobile's default alarm sound
     *
     * @param newAlarmSound New default alarm sound
     * @return {@code newAlarmSound} with updated property {@link AlarmSound#isAlarm()} set to {@code true}
     * @throws AlarmSoundAdapterException Setting new alarm sound failed
     */
    AlarmSound setDefaultAlarmSound(AlarmSound newAlarmSound);

    AlarmSound getCurrentAlarmSound();

    /**
     * Remove alarmSound from the list of alarm sounds
     * <p>
     * The sound file itself will stay untouched and is not deleted.
     * It's just unflagged that it's no alarm sound anymore.
     *
     * @param alarmSound Alarm sound to be removed from alarm sounds list
     * @throws AlarmSoundAdapterException Removing the alarm sound failed
     */
    void removeAlarmSound(AlarmSound alarmSound);
}
