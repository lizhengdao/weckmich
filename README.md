# WeckMich

This Android app lets you randomly select a new alarm sound from your existing sound files.

Ever tried to pick a song to use it as alarm sound in Anroid? Choose it in the clock app or the settings app - either way you need many taps to finally select a song to be used as alarm sound.  
WeckMich lets you do that with just one tap!

<table>
        <tr>
            <th>Pick a random song</th>
            <th>Preview...and select</th>
        </tr>
        <tr>
            <td style="text-align:center"><img src="img/screenshot1.jpg" width=80%/></td>
            <td style="text-align:center"><img src="img/screenshot2.jpg" width=80%/></td>
        </tr>
    </table>

## Prerequisites
- You have to set the alarm sound to `default` - this will be whatever alarm sound is picked by WeckMich
- Since WeckMich sets the alarm sound in the settings, it needs the permission to change the settings :worried:


## Credits
This projects uses following other great projects:
- Generation of `Parcelable` implementing classes: https://github.com/rharter/auto-value-parcel
- Equalizer visualization: https://github.com/HugoGresse/AndroidVuMeter
- Icons: https://material.io/resources/icons/?style=baseline


## License
[MIT](https://choosealicense.com/licenses/mit/)